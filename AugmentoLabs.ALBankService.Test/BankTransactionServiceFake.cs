﻿using AugmentoLabs.ALBankService.Business;
using AugmentoLabs.ALBankService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AugmentoLabs.ALBankService.Test
{
    public class BankTransactionServiceFake : IBankTransaction
    {
        private readonly List<BankTransaction> _bankTransaction;
        public BankTransactionServiceFake()
        {
            _bankTransaction = new List<BankTransaction>()
            {
                new BankTransaction
                {
                    TransactionId= new Guid(),
                    AccountNumber=10000001,
                    AccountBalance=100,
                    Deposit=10,
                    Status=true,
                    TransactionDateTime=DateTime.Now,
                    Withdrawal=20,
                    TransactionType=TxnType.Deposit
                },
                new BankTransaction
                {
                    TransactionId= new Guid(),
                    AccountNumber=10000002,
                    AccountBalance=150,
                    Deposit=10,
                    Status=true,
                    TransactionDateTime=DateTime.Now,
                    Withdrawal=20,
                    TransactionType=TxnType.Deposit
                }
            };
        }
        public async Task<BankTransactionViewModel> DepostAsync(BankTransaction transaction)
        {
            transaction.TransactionId = new Guid();
            _bankTransaction.Add(transaction);
            return new BankTransactionViewModel
            {
                TransactionId = transaction.TransactionId,
                AccountBalance = transaction.AccountBalance,
                Status = true,
                TxnDateTime = DateTime.Now,
                TransactionType = TxnType.Deposit
            };
        }

        public IEnumerable<BankTransactionViewModel> GetMiniStatement(long accountNumber)
        {
            var result = _bankTransaction.Where(t => t.AccountNumber == accountNumber).OrderBy(a => a.TransactionDateTime).Take(5);
            return null;    
        }

        public IEnumerable<BankTransactionViewModel> GetTransactionDetails(long AccountNumber, DateTime startDate,DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public Task<BankTransactionViewModel> WithdrawalAsync(BankTransaction transaction)
        {
            throw new NotImplementedException();
        }
    }
}
