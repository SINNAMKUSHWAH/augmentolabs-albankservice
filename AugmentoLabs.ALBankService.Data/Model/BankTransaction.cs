﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AugmentoLabs.ALBankService.Data
{
    [Table("ALBank.BankTransaction")]
    public class BankTransaction
    {
        [Key]
        public Guid TransactionId { get; set; }

        [Required]
        [ForeignKey("AccountNumber")]
        public long AccountNumber { get; set; }

        [Required]
        public TxnType TransactionType { get; set; }
        
        public decimal AccountBalance { get; set; } = 0;

        //[Range(double.Epsilon, double.MaxValue)]
        public decimal Deposit { get; set; }

        //[Range(double.Epsilon, double.MaxValue)]
        public decimal Withdrawal { get; set; }

        public bool Status { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime TransactionDateTime { get; set; }

    }

    public enum TxnType { Deposit = 1, Withdrawal };
}
