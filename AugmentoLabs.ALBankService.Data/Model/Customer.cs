﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AugmentoLabs.ALBankService.Data
{
    [Table("ALBank.Customer")]
    public class Customer
    {
        private string _password;

        [Key]
        [StringLength(5)]
        public string CustomerId { get; set; }

        [MinLength(3, ErrorMessage = "First name should be greater than 3!!!")]
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required]
        public Gender Gender { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public long Mobile { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }
        [Required]
        [MaxLength(150, ErrorMessage = "Address should not be more than 150 char!!!")]
        public string Address { get; set; }
        [Required]

        public long AccountNumber { get; set; }

        [Required]
        [MinLength(5, ErrorMessage = "User id must be 5 character")]
        public string UserId { get; set; }
        [Required]
        [MinLength(5, ErrorMessage = "Password  must be 5 character")]
        public string Password
        {
            get => Base64Decode(_password);
            set => _password = Base64Encode(value);
        }
        public bool IsActive { get; set; } = false;

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }

        public string CreatedById { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime ModifiedDate { get; set; }

        public string ModifiedById { get; set; }

        [ForeignKey("CustomerRoleId")]
        public string CustomerRoleId { get; set; }
        public virtual CustomerRole CustomerRole { get; set; }

        //To Encrypt the password
        public static string Base64Encode(string str) => Convert.ToBase64String(Encoding.UTF8.GetBytes(str));

        private static string Base64Decode(string str) => Encoding.UTF8.GetString(Convert.FromBase64String(str));

    }
    public enum Gender { Male = 1, Female };
}
