﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AugmentoLabs.ALBankService.Data
{
    [Table("ALBank.CustomerRole")]
    public class CustomerRole
    {
        [Key]
        [StringLength(5)]
        public string CustomerRoleId { get; set; }

        [MinLength(5,ErrorMessage ="Role name should be greater than 5!!!")]
        [Required]
        public string RoleName { get; set; }
        public string CreatedById { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedByDate { get; set; }
        public string ModifiedById { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime ModifiedByDate { get; set; }
        public bool IsActive { get; set; }
        
    }
}
