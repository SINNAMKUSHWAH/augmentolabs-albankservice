﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AugmentoLabs.ALBankService.Data.Migrations
{
    public partial class Initial_ALBank : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ALBank.CustomerRole",
                columns: table => new
                {
                    CustomerRoleId = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: false),
                    RoleName = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    CreatedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedByDate = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    ModifiedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedByDate = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ALBank.CustomerRole", x => x.CustomerRoleId);
                });

            migrationBuilder.CreateTable(
                name: "ALBank.Customer",
                columns: table => new
                {
                    CustomerId = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Gender = table.Column<int>(type: "int", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Mobile = table.Column<long>(type: "bigint", nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    AccountNumber = table.Column<long>(type: "bigint", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    ModifiedById = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CustomerRoleId = table.Column<string>(type: "nvarchar(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ALBank.Customer", x => x.CustomerId);
                    table.ForeignKey(
                        name: "FK_ALBank.Customer_ALBank.CustomerRole_CustomerRoleId",
                        column: x => x.CustomerRoleId,
                        principalTable: "ALBank.CustomerRole",
                        principalColumn: "CustomerRoleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ALBank.BankTransaction",
                columns: table => new
                {
                    TransactionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AccountNumber = table.Column<long>(type: "bigint", nullable: false),
                    TransactionType = table.Column<int>(type: "int", nullable: false),
                    AccountBalance = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Deposit = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Withdrawal = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    TransactionDateTime = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CustomerId = table.Column<string>(type: "nvarchar(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ALBank.BankTransaction", x => x.TransactionId);
                    table.ForeignKey(
                        name: "FK_ALBank.BankTransaction_ALBank.Customer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "ALBank.Customer",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ALBank.BankTransaction_CustomerId",
                table: "ALBank.BankTransaction",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_ALBank.Customer_AccountNumber",
                table: "ALBank.Customer",
                column: "AccountNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ALBank.Customer_CustomerRoleId",
                table: "ALBank.Customer",
                column: "CustomerRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ALBank.Customer_Email",
                table: "ALBank.Customer",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ALBank.Customer_UserId",
                table: "ALBank.Customer",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ALBank.CustomerRole_RoleName",
                table: "ALBank.CustomerRole",
                column: "RoleName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ALBank.BankTransaction");

            migrationBuilder.DropTable(
                name: "ALBank.Customer");

            migrationBuilder.DropTable(
                name: "ALBank.CustomerRole");
        }
    }
}
