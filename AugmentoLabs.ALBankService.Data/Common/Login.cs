﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AugmentoLabs.ALBankService.Data
{
    public class Login
    {
        public string UserId { get; set; }

        public string Password { get; set; }
    }
}
