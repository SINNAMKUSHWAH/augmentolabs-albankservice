﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AugmentoLabs.ALBankService.Data
{

    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    sealed class EncryptedAttribute : Attribute
    {
        readonly string _fieldName;

        public EncryptedAttribute(string fieldName)
        {
            _fieldName = fieldName;
        }

        public string FieldName => _fieldName;
    }
}
