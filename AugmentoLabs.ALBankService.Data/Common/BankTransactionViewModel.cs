﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AugmentoLabs.ALBankService.Data
{
    public class BankTransactionViewModel
    {
        public Guid TransactionId { get; set; }
        public DateTime TxnDateTime { get; set; }
        public TxnType TransactionType { get; set; }
        public decimal Deposit { get; set; }
        public decimal Withdrawal { get; set; }
        public decimal AccountBalance { get; set; }
        public bool Status { get; set; }
    }
}
