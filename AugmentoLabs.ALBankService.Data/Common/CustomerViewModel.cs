﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AugmentoLabs.ALBankService.Data
{
    public class CustomerViewModel
    {
        public string CustomerId { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }

        public string SecretKey { get; set; }

    }
}
