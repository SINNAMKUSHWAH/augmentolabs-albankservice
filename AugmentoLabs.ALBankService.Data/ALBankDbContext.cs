﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AugmentoLabs.ALBankService.Data
{
    public class ALBankDbContext : DbContext
    {
        public ALBankDbContext(DbContextOptions<ALBankDbContext> options) : base(options)
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerRole> CustomerRoles { get; set; }

        public DbSet<BankTransaction> BankTransactions { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Customer>()
            .Property(s => s.CreatedDate)
            .HasDefaultValueSql("GETDATE()");

            builder.Entity<Customer>()
            .Property(s => s.ModifiedDate)
            .HasDefaultValueSql("GETDATE()");

            builder.Entity<CustomerRole>()
            .Property(s => s.CreatedByDate)
            .HasDefaultValueSql("GETDATE()");

            builder.Entity<CustomerRole>()
            .Property(s => s.ModifiedByDate)
            .HasDefaultValueSql("GETDATE()");

            builder.Entity<BankTransaction>()
            .Property(s => s.TransactionDateTime)
            .HasDefaultValueSql("GETDATE()");


            builder.Entity<Customer>(entity =>
            {
                entity.HasIndex(e => e.Email).IsUnique();
                entity.HasIndex(e => e.UserId).IsUnique();
                entity.HasIndex(e => e.AccountNumber).IsUnique();
            });

            builder.Entity<CustomerRole>(entity =>
            {
                entity.HasIndex(e => e.RoleName).IsUnique();
            });
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies(true);
        }
    }
}
