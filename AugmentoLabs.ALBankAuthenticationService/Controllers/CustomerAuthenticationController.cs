﻿using AugmentoLabs.ALBankService.Business;
using AugmentoLabs.ALBankService.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AugmentoLabs.ALBankAuthenticationService.Controllers
{
    [Route("ALBank/[controller]")]
    [ApiController]
    public class CustomerAuthenticationController : ControllerBase
    {
        private ICustomerAuthentication _customerAuthentication;
        public CustomerAuthenticationController(ICustomerAuthentication customerAuthentication)
        {
            _customerAuthentication = customerAuthentication;
        }

        public IActionResult AddCustomer([FromBody] Login login)
        {
            var cust = _customerAuthentication.CustomerAuthernticate(login.UserId, login.Password);
            if (cust == null)
                return BadRequest(new { message = "Invalid userId or password!!!" });
            return Ok(cust);

        }
    }
}
