﻿using AugmentoLabs.ALBankService.Business;
using AugmentoLabs.ALBankService.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AugmentoLabs.ALBankAuthenticationService.Controllers
{
    [Route("ALBank/[controller]")]
    [ApiController]
    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CustomerController : ControllerBase
    {
        private ICustomer customer;
        public CustomerController(ICustomer customer)
        {
            this.customer = customer;
        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public ActionResult GetCustomers()
        {
            var customers = customer.GetCustomers();
            if (customer == null)
                return BadRequest("No customers are active at this moment!!!");

            return Ok(customer);
        }
        [HttpPost]
        public async Task<IActionResult> AddCustomer(Customer cust)
        {
            var result = await customer.AddCustomerAsync(cust);
            return Ok(result);
        }
        [HttpPut]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> UpdateUser(Customer cust)
        {
            var result = await customer.UpdateCustomerAsync(cust);
            return Ok(result);
        }
        [HttpPut]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> UpdateUser([FromBody] CustomerViewModel cust)
        {
            if (GetUserRole() == "admin")
            {
                var result = await customer.UpdateCustomerStatusAsync(true, cust.CustomerId);
                return Ok(result);
            }
            return BadRequest("You don't have permission to modify the customer status.");
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteCustomer(string userId)
        {
            
            if (GetUserRole() == "admin")
            {
                var result = await customer.DeleteCustomerAsync(userId);
                return Ok(result);
            }
            return BadRequest("You don't have permission to delete the customer status.");
        }
        private string GetUserRole()
        {
            var identity = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<System.Security.Claims.Claim> claims = identity.Claims;
                foreach (var c in claims)
                {
                    if (c.Type.Contains("role"))
                    {
                        return c.Value;
                    }
                }

            }
            return null;
        }
    }
}
