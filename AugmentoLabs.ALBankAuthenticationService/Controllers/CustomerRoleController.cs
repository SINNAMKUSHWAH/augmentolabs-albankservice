﻿using AugmentoLabs.ALBankService.Business;
using AugmentoLabs.ALBankService.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AugmentoLabs.ALBankAuthenticationService.Controllers
{
    [Route("ALBank/[controller]")]
    [ApiController]
    public class CustomerRoleController : ControllerBase
    {

        private ICustomerRole customerRole;
        public CustomerRoleController(ICustomerRole customerRole)
        {
            this.customerRole = customerRole;
        }

        [HttpPost]
        public async Task<IActionResult> AddCustomerRole(CustomerRole custRole)
        {
            var result = await customerRole.AddRoleAsync(custRole);
            return Ok(result);
        }
        [HttpPut]
        public async Task<IActionResult> UpdateCustomerRole(CustomerRole custRole)
        {
            var result = await customerRole.UpdateRoleAsync(custRole);
            return Ok(result);
        }
    }
}
