﻿using AugmentoLabs.ALBankService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AugmentoLabs.ALBankService.Business
{
    public class CustomerService : ICustomer
    {
        private readonly ALBankDbContext aLBankDbContext;

        public CustomerService(ALBankDbContext aLBankDbContext)
        {
            this.aLBankDbContext = aLBankDbContext;
        }
        public async Task<Customer> AddCustomerAsync(Customer customer)
        {
            customer.CreatedById = customer.CreatedById == null ? customer.CustomerId : customer.CreatedById;
            customer.ModifiedById = customer.ModifiedById == null ? customer.CustomerId : customer.ModifiedById;
            customer.AccountNumber = GetAccountNum(customer.CustomerId);
            try
            {
                await aLBankDbContext.AddAsync(customer);
                await aLBankDbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return customer;
        }

        public async Task<Customer> DeleteCustomerAsync(string userId)
        {
            var customer = aLBankDbContext.Customers.Where(c => c.UserId == userId).FirstOrDefault();
            if (customer != null)
            {
                aLBankDbContext.Customers.Remove(customer);
                await aLBankDbContext.SaveChangesAsync();
            }
            return customer;
        }

        public IEnumerable<Customer> GetCustomers()
        {
            return aLBankDbContext.Customers;
        }

        public async Task<Customer> UpdateCustomerAsync(Customer cust)
        {
            var customer = aLBankDbContext.Customers.Where(c => c.UserId == cust.UserId).FirstOrDefault();
            if (customer != null)
            {
                aLBankDbContext.Customers.Update(cust);
                await aLBankDbContext.SaveChangesAsync();
            }
            return customer;
        }

        public async Task<bool> UpdateCustomerStatusAsync(bool status,string userId )
        {
            var customer = aLBankDbContext.Customers.Where(c => c.UserId == userId).FirstOrDefault();
            if (customer != null)
            {
                customer.IsActive = status;
                aLBankDbContext.Customers.Update(customer);
                await aLBankDbContext.SaveChangesAsync();
            }
            return true;
        }
        private long GetAccountNum(string custId)
        {
            if (custId.Length > 3)
            {
                var acc = Convert.ToInt64(custId.Substring(3));
                return 10000000 + acc;
            }
            return 0;
        }
    }
}
