﻿using AugmentoLabs.ALBankService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AugmentoLabs.ALBankService.Business
{
    public class BankTransactionService : IBankTransaction
    {
        private readonly ALBankDbContext aLBankDbContext;
        public BankTransactionService(ALBankDbContext aLBankDbContext)
        {
            this.aLBankDbContext = aLBankDbContext;
        }
        public async Task<BankTransactionViewModel> DepostAsync(BankTransaction trans)
        {
            if (aLBankDbContext.BankTransactions.Count() > 0)
            {
                var txn = aLBankDbContext.BankTransactions.Where(txn => txn.AccountNumber == trans.AccountNumber).FirstOrDefault();
                if (txn != null)
                {
                    txn.AccountBalance += trans.Deposit;
                    trans.AccountBalance = txn.AccountBalance;
                    await aLBankDbContext.AddAsync(trans);
                    await aLBankDbContext.SaveChangesAsync();

                    return new BankTransactionViewModel()
                    {
                        TransactionId = trans.TransactionId,
                        TransactionType = TxnType.Deposit,
                        TxnDateTime = trans.TransactionDateTime,
                        AccountBalance = trans.AccountBalance,
                        Status = true
                    };
                }
            }
            else
            {
                var customer = aLBankDbContext.Customers.Where(txn => txn.AccountNumber == trans.AccountNumber).FirstOrDefault();
                if (customer != null)
                {
                    trans.AccountBalance += trans.Deposit;
                    await aLBankDbContext.AddAsync(trans);
                    await aLBankDbContext.SaveChangesAsync();

                    return new BankTransactionViewModel()
                    {
                        TransactionId = trans.TransactionId,
                        TransactionType = TxnType.Deposit,
                        TxnDateTime = trans.TransactionDateTime,
                        AccountBalance = trans.AccountBalance,
                        Status = true
                    };
                }
            }
            return null;
        }

        public async Task<BankTransactionViewModel> WithdrawalAsync(BankTransaction tran)
        {
            //var txn = aLBankDbContext.BankTransactions.Where(txn => txn.Customer.AccountNumber == tran.AccountNumber && txn.Customer.IsActive).FirstOrDefault();
            var cust = aLBankDbContext.Customers.Where(c => c.AccountNumber == tran.AccountNumber).FirstOrDefault();
            if (aLBankDbContext.BankTransactions.Count() > 0)
            {
                var txn = aLBankDbContext.BankTransactions.Where(txn => txn.AccountNumber == tran.AccountNumber).FirstOrDefault();
                if (txn != null && txn.AccountBalance > tran.Withdrawal)
                {
                    txn.AccountBalance -= tran.Withdrawal;
                    tran.AccountBalance = txn.AccountBalance;
                    await aLBankDbContext.AddAsync(tran);
                    await aLBankDbContext.SaveChangesAsync();
                    return new BankTransactionViewModel()
                    {
                        TransactionId = txn.TransactionId,
                        TransactionType = TxnType.Withdrawal,
                        TxnDateTime = txn.TransactionDateTime,
                        AccountBalance = txn.AccountBalance,
                        Status = true
                    };
                }
            }
            return null;
        }

        public IEnumerable<BankTransactionViewModel> GetMiniStatement(long accountId)
        {
            var result = aLBankDbContext.BankTransactions.Where(a => a.AccountNumber == accountId).OrderByDescending(t => t.TransactionDateTime).Take(5);
            if (result != null)
            {
                return ConvertToBankViewModel(result);
            }
            return null;

        }

        public IEnumerable<BankTransactionViewModel> GetTransactionDetails(long accountId, DateTime startDate, DateTime endDate)
        {
            var result = aLBankDbContext.BankTransactions.Where(a => a.AccountNumber == accountId && a.TransactionDateTime >= startDate && a.TransactionDateTime <= endDate).OrderByDescending(t => t.TransactionDateTime);
            if (result != null)
            {
                return ConvertToBankViewModel(result);
            }

            return null;
        }
        private IEnumerable<BankTransactionViewModel> ConvertToBankViewModel(IQueryable<BankTransaction> bankTransactions)
        {
            var bankTransactionViewModels = new List<BankTransactionViewModel>();
            foreach (var b in bankTransactions)
            {
                bankTransactionViewModels.Add(

                    new BankTransactionViewModel
                    {
                        TxnDateTime = b.TransactionDateTime,
                        TransactionId = b.TransactionId,
                        TransactionType = b.TransactionType,
                        Deposit = b.Deposit,
                        Withdrawal = b.Withdrawal,
                        Status = b.Status,
                        AccountBalance = b.AccountBalance

                    });
            }
            return bankTransactionViewModels;
        }

    }
}
