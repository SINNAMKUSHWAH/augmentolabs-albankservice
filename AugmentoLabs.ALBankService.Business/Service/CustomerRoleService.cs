﻿using AugmentoLabs.ALBankService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AugmentoLabs.ALBankService.Business
{
    public class CustomerRoleService : ICustomerRole
    {
        private readonly ALBankDbContext aLBankDbContext;

        public CustomerRoleService(ALBankDbContext aLBankDbContext)
        {
            this.aLBankDbContext = aLBankDbContext;
        }
        public async Task<bool> AddRoleAsync(CustomerRole customerRole)
        {

            await aLBankDbContext.CustomerRoles.AddAsync(customerRole);
            await aLBankDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateRoleAsync(CustomerRole customerRole)
        {
            var role = aLBankDbContext.CustomerRoles.Where(r => r.CustomerRoleId == customerRole.CustomerRoleId).FirstOrDefault();
            if (role != null)
            {
                aLBankDbContext.CustomerRoles.Update(customerRole);
                await aLBankDbContext.SaveChangesAsync();
            }
            return true;
        }
    }
}
