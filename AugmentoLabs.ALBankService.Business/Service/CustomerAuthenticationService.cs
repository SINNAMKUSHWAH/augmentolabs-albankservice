﻿using AugmentoLabs.ALBankService.Data;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace AugmentoLabs.ALBankService.Business
{
    public class CustomerAuthenticationService : ICustomerAuthentication
    {
        private readonly ALBankDbContext _aLBankDbContext;
        private readonly AppSettings _appSettings;
        public CustomerAuthenticationService(IOptions<AppSettings> appSettings, ALBankDbContext aLBankDbContext)
        {
            _appSettings = appSettings.Value;
            _aLBankDbContext = aLBankDbContext;
        }

        public CustomerViewModel CustomerAuthernticate(string userId, string password)
        {
            var pwd = Customer.Base64Encode(password);
            var customer = _aLBankDbContext.Customers.Where(cust => cust.UserId == userId && cust.Password == pwd).FirstOrDefault();
            if (customer == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var secretKey = Encoding.ASCII.GetBytes(_appSettings.SecureKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
            new Claim(ClaimTypes.Name,customer.UserId),
            new Claim(ClaimTypes.Role,customer.CustomerRole.RoleName),
            new Claim(ClaimTypes.Version,"V3.1")
            }),
                Expires = DateTime.Now.AddMinutes(10),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKey), SecurityAlgorithms.HmacSha384Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            // user.PrivateKey = tokenHandler.WriteToken(token);
            var result = new CustomerViewModel
            {
                CustomerId = customer.UserId,
                Name = customer.FirstName + " " + customer.LastName,
                SecretKey = tokenHandler.WriteToken(token),
                Role = customer.CustomerRole.RoleName
            };

            return result;

        }
    }
}
