﻿using AugmentoLabs.ALBankService.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AugmentoLabs.ALBankService.Business
{
    public interface ICustomer
    {
        IEnumerable<Customer> GetCustomers();

        Task<Customer> AddCustomerAsync(Customer user);

        Task<Customer> UpdateCustomerAsync(Customer user);
        Task<bool> UpdateCustomerStatusAsync(bool status, string userId);

        Task<Customer> DeleteCustomerAsync(string userId);
    }
}
