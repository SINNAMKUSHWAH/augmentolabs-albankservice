﻿using AugmentoLabs.ALBankService.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AugmentoLabs.ALBankService.Business
{
    public interface IBankTransaction
    {
        Task<BankTransactionViewModel> DepostAsync(BankTransaction transaction);

        Task<BankTransactionViewModel> WithdrawalAsync(BankTransaction transaction);

        IEnumerable<BankTransactionViewModel> GetMiniStatement(long accountNumber);

        IEnumerable<BankTransactionViewModel> GetTransactionDetails(long AccountNumber, DateTime startDate, DateTime endDate);
    }
}
