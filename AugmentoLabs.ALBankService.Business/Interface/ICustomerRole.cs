﻿using AugmentoLabs.ALBankService.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AugmentoLabs.ALBankService.Business
{
    public interface ICustomerRole
    {
        Task<bool> AddRoleAsync(CustomerRole customerRole);
        Task<bool> UpdateRoleAsync(CustomerRole customerRole);
    }
}
