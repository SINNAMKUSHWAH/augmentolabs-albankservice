﻿using AugmentoLabs.ALBankService.Business;
using AugmentoLabs.ALBankService.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;

namespace AugmentoLabs.ALBankService.Controllers
{
    [Route("ALBank/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BankTransactionController : ControllerBase
    {
        private readonly IBankTransaction bankTransaction;
        public BankTransactionController(IBankTransaction bankTransaction)
        {
            this.bankTransaction = bankTransaction;
        }

        [HttpPost]
        public async Task<ActionResult> DepositWithdrawal(BankTransaction bankTxn)
        {
            if (bankTxn.TransactionType == TxnType.Deposit || bankTxn.TransactionType == TxnType.Withdrawal)
            {


                dynamic result;
                if (TxnType.Deposit == bankTxn.TransactionType)
                {
                    result = await bankTransaction.DepostAsync(bankTxn);
                    if (result == null)
                        return BadRequest("Transaction is fail.Please check account number.");
                    return Ok(result);
                }

                result = await bankTransaction.WithdrawalAsync(bankTxn);
                if (result == null)
                    return BadRequest("Transaction is fail. Please check your available balance or account number");
                return Ok(result);
            }
            return BadRequest("Please specify the transaction type, Deposit or Withdrawal.");

        }

        //[HttpGet("accountNumber={accountNumber}&startDate={startDate}&endDate={endDate}")]
        //public ActionResult GetTransactionsDetail(long accountNumber, string startDate, string endDate)
        public ActionResult GetTransactionsDetail([FromQuery] long accountNumber, [FromQuery] string startDate, [FromQuery] string endDate)
        {
            return Ok(bankTransaction.GetTransactionDetails(accountNumber, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate)));

        }

        [HttpGet("accountNumber={accountNumber}")]
        public ActionResult GetMiniStatement(long accountNumber)
        {
            return Ok(bankTransaction.GetMiniStatement(accountNumber));
        }
    }
}
